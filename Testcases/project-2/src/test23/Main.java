package test23;

/*
 * @author Sridhar Gopinath		-		g.sridhar53@gmail.com
 * 
 * Course project,
 * Principles of Programming Course, Fall - 2016,
 * Computer Science and Automation (CSA),
 * Indian Institute of Science (IISc),
 * Bangalore
 */

/*
 * Test 1: Straight line code
 */

public class Main {

	public static void main(String[] args) {

		System.out.println("Hello world");

		return;
	}

}
